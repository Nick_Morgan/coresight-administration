import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import { fetchAccessToken } from '../actions';

/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */

const styles = {
  title: {
    cursor: 'pointer',
  },
};

class TopBar extends Component {
  componentDidMount() {
    this.props.fetchAccessToken();
  }
  render() {
    console.log(this.props);
    return(
      <AppBar
        title={<span style={styles.title}>LookNorth Administration</span>}
        showMenuIconButton = {false}
        style={{backgroundColor: '#03A9F4'}}
      />
    );
  }
}

function mapStatetoProps(state) {
  return { posts: state.posts }
}


export default connect(mapStatetoProps, { fetchAccessToken })(TopBar);
