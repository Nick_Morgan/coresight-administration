import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TopBar from './AppBar';

export default class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <TopBar/>
      </MuiThemeProvider>
    );
  }
}
