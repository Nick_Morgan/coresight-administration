import axios from 'axios';
import {
  AUDIENCE,
  AUTH0_ACCESS_TOKEN_URL,
  AUTH0_USERS_URL,
  CLIENT_ID,
  CLIENT_SECRET,
  GRANT_TYPE
} from '../config/constants';

const options = {
  method: 'POST',
  body: JSON.stringify({
    audience: AUDIENCE,
    client_id: CLIENT_ID,
    client_secret: CLIENT_SECRET,
    grant_type: GRANT_TYPE
  }),
  headers: new Headers({ 'Content-Type': 'application/json' })
};

export const FETCH_USERS = 'FETCH_USERS';

export function fetchAccessToken() {
  const request = axios.get(AUTH0_ACCESS_TOKEN_URL,options);
  const json = response.json();

  return {
    type: FETCH_USERS,
    payload: json
  };
}
